# flask-template

Flask+SQLAlchemy+Docker project template

# Migrations

```shell
docker exec -it flask-app flask db init
docker exec -it flask-app flask db migrate -m "Initial migration."
```
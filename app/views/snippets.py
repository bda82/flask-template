from flask import Blueprint, render_template

snippets_bp = Blueprint('snippets', __name__, )


@snippets_bp.route("/snippets")
def snippets():
    return render_template('layouts/snippets.html')

from flask import Blueprint, render_template

notes_bp = Blueprint('notes', __name__, )


@notes_bp.route("/notes")
def notes():
    notes = [
        {
            "id": 1,
            "title": "First",
            "description": "First description",
            "body": "First body",
        },
        {
            "id": 2,
            "title": "Second",
            "description": "Second description",
            "body": "Second body",
        },
        {
            "id": 3,
            "title": "Third",
            "description": "Third description",
            "body": "Third body",
        }
    ]
    active_note = {
        "id": 1,
        "title": "First",
        "description": "First description",
        "body": "First body",
    }
    return render_template('layouts/notes.html', notes=notes, active_note=active_note)

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_debugtoolbar import DebugToolbarExtension
from flask_migrate import Migrate

from config import Config
from views.index import index_bp
from views.notes import notes_bp
from views.snippets import snippets_bp

app = Flask(__name__)

app.config.from_object('config.DevelopConfig')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app=app)

migrate = Migrate(app, db)

dtb = DebugToolbarExtension(app)

app.register_blueprint(index_bp)
app.register_blueprint(notes_bp)
app.register_blueprint(snippets_bp)


if __name__ == "__main__":
    app.run(host=Config.SERVER_HOST, port=Config.SERVER_PORT)

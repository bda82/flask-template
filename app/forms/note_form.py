from flask_wtf import Form
from wtforms import StringField
from wtforms.validators import DataRequired


class NoteForm(Form):
    title = StringField(validators=[DataRequired,])
    description = StringField()
    body = StringField()

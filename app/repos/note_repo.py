from repos.base_repo import BaseRepo
from models.note import Note


class NoteRepo(BaseRepo):
    model = Note

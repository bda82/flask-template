import typing as t
import logging
from abc import ABC
from datetime import datetime

from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

T = t.TypeVar("T")


class BaseRepo(ABC):
    model: type(T) = NotImplemented

    def __init__(self, db: Session):
        self.db = db
        self.name = self.model.__name__
        self._logger = logging.getLogger(self.name)

    @staticmethod
    def is_null_error(e: Exception):
        return "null value in column" in str(e)

    @staticmethod
    def is_check_constraint_error(e: Exception):
        return "violates check constraint" in str(e)

    @staticmethod
    def check_constraint_error(e: Exception):
        return "violates check constraint" in str(e)

    def save(self, obj: T) -> T:
        self.db.add(obj)
        try:
            self.db.commit()
        except IntegrityError as e:
            self.db.rollback()

            if self.is_null_error(e):
                raise Exception("Required fields are empty")
            if self.check_constraint_error(e):
                raise Exception("Check constraint error")

            raise

        return obj

    def mutate(self, **kwargs) -> dict:
        return kwargs

    def delete(self, pk: t.Union[int, str]):
        obj = self.get(pk, silent=False)
        self.db.delete(obj)
        try:
            self.db.commit()
        except IntegrityError as e:
            self.db.rollback()
            msg = f"Cannot delete {self.name}:{pk}"
            self._logger.warning(f"{msg} Original:{e}")
            raise Exception(msg)

        self._logger.info("Deleted %s:%s", self.name, pk)

        return obj

    def get(
        self, pk: t.Union[int, str, list], silent: bool = True
    ) -> t.Union[T, t.Iterable[T], None]:
        if pk and isinstance(pk, list):
            res = self.db.query(self.model).filter(self.model.id.in_(pk)).all()
        elif pk:
            res = self.db.query(self.model).get(pk)
        else:
            raise Exception("Empty id")

        if not (res or silent):
            raise Exception(f"Does not exist {self.name}:{pk}")

        return res

    def create(self, **kwargs) -> T:
        data = self.mutate(**kwargs)
        obj = self.model(**data)

        self.save(obj)
        self.db.refresh(obj)

        self._logger.info("Created %s:%s", self.name, obj.id)

        return obj

    def update(
        self,
        pk: t.Union[int, str],
        ignore_unset: bool = False,
        **kwargs,
    ) -> T:
        obj = self.get(pk, silent=False)

        if hasattr(obj, "updated_at"):
            obj.updated_at = datetime.utcnow()

        data = self.mutate(**kwargs)
        for field, value in data.items():
            if value is not None or not ignore_unset:
                setattr(obj, field, value)

        self.save(obj)

        self.db.refresh(obj)
        return obj

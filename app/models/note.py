from app import db


class Note(db.Model):
    __tablename__ = "note"

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), nullable=False)
    description = db.Column(db.String(255), nullable=False)
    body = db.Column(db.String(255), nullable=False)

    def __repr__(self):
        return '<Note %r>' % self.title

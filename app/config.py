import os

MYSQL_HOST = os.environ.get("MYSQL_HOST", "flask-db")
MYSQL_DATABASE = os.environ.get("MYSQL_DATABASE", "flask")
MYSQL_USER = os.environ.get("MYSQL_USER", "flask")
MYSQL_PASSWORD = os.environ.get("MYSQL_PASSWORD", "password")
SECRET_KEY = os.environ.get("SECRET_KEY", "flask-insecure-%hbm7i%=w#hk*@oh5-wc_qn!dd7qdm40+p07+7@mdr8m@8rnpr")

DATABASE_URL = f"mysql+mysqlconnector://{MYSQL_USER}:{MYSQL_PASSWORD}@{MYSQL_HOST}/{MYSQL_DATABASE}"


class Config:
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = SECRET_KEY
    SQLALCHEMY_DATABASE_URI = DATABASE_URL
    SERVER_HOST = 'localhost'
    SERVER_PORT = 5000


class ProductionConfig(Config):
    DEBUG = False


class DevelopConfig(Config):
    DEBUG = True
    ASSETS_DEBUG = True
    SQLALCHEMY_ECHO = True

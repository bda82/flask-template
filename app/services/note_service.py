import typing as t
import dataclasses

from sqlalchemy.orm import Session

from repos.note_repo import NoteRepo


@dataclasses.dataclass
class NoteService:
    _repo: NoteRepo

    def get(self, pk: t.Union[int, str, list], silent: bool = True):
        return self._repo.get(pk, silent)


def get_note_service(db: Session):
    return NoteService(_repo=NoteRepo(db))
